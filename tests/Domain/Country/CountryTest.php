<?php

declare(strict_types=1);

namespace Tests\Domain\Country;

use App\Domain\Country\Country;
use Tests\TestCase;

class CountryTest extends TestCase
{
    public function countryProvider()
    {
        return [
            ["no", "Norway", "Oslo", "Europe", 5176998, [62, 10]],
            ["es", "Spain", "Madrid", "Europe", 46439864, [40, -4]],
            ["jp", "Japan", "Tokyo", "Asia", 126865000, [36, 138]],
            ["cl", "Chile", "Santiago", "Americas", 18006407, [-30, -71]]
        ];
    }

    public function countryObjectProvider()
    {
        return [
            [
                new Country("c1", "Country1", "Capital1", "Region1", 200, [0, 0]),
                new Country("c2", "Country2", "Capital2", "Region2", 100, [0, 0]),
                1
            ],
            [
                new Country("c1", "Country1", "Capital1", "Region1", 100, [0, 0]),
                new Country("c2", "Country2", "Capital2", "Region2", 100, [0, 0]),
                0
            ],
            [
                new Country("c1", "Country1", "Capital1", "Region1", 100, [0, 0]),
                new Country("c2", "Country2", "Capital2", "Region2", 200, [0, 0]),
                -1
            ],
        ];
    }

    /**
     * @dataProvider countryProvider
     * @param string $code
     * @param string $name
     * @param string $capital
     * @param string $region
     * @param int    $population
     * @param array  $latlng
     */
    public function testGetters(string $code, string $name, string $capital, string $region, int $population, array $latlng)
    {
        $country = new Country($code, $name, $capital, $region, $population, $latlng);

        $this->assertEquals($code, $country->getCode());
        $this->assertEquals($name, $country->getName());
        $this->assertEquals($capital, $country->getCapital());
        $this->assertEquals($population, $country->getPopulation());
        $this->assertEquals($latlng, $country->getLatlng());
    }

    /**
     * @dataProvider countryProvider
     * @param string $code
     * @param string $name
     * @param string $capital
     * @param string $region
     * @param int    $population
     * @param array  $latlng
     */
    public function testJsonSerialize(string $code, string $name, string $capital, string $region, int $population, array $latlng)
    {
        $country = new Country($code, $name, $capital, $region, $population, $latlng);

        $expectedPayload = json_encode([
            'code' => $code,
            'name' => $name,
            'capital' => $capital,
            'region' => $region,
            'population' => $population,
            'latlng' => $latlng,
        ]);

        $this->assertEquals($expectedPayload, json_encode($country));
    }

    /**
     * @dataProvider countryObjectProvider
     * @param Country $country1
     * @param Country $country2
     * @param int $expectedResult
     */
    public function testGreaterPopulationThan(Country $country1, Country $country2, int $expectedResult)
    {
        $this->assertEquals($country1->greaterPopulationThan($country2), $expectedResult);
    }
}
