<?php

declare(strict_types=1);

namespace Tests\Domain\Country;

use App\Domain\Country\Country;
use App\Domain\Country\CountryCriteria;
use Tests\TestCase;

class CountryCriteriaTest extends TestCase
{

    /**
     * @dataProvider countryCriteriaProvider
     * @param Country   $country
     * @param string    $region
     * @param Country   $rivalCountry
     */
    public function testGetters(Country $country, string $region = "Region", string $rivalCode = "c1")
    {
        $countryCriteria = new CountryCriteria($country, $region, $rivalCode);

        $this->assertEquals($rivalCode, $countryCriteria->getRivalCode());
    }

    /**
     * @dataProvider codeCriteriaProvider
     * @param Country   $country
     * @param bool      $expectedResult
     */
    public function testCodeCriteria(Country $country, bool $expectedResult)
    {
        $countryCriteria = new CountryCriteria($country);

        $this->assertEquals($countryCriteria->checkCodeCriteria(), $expectedResult);
    }

    /**
     * @dataProvider regionCriteriaProvider
     * @param Country   $country
     * @param bool      $expectedResult
     * @param region|null    $region
     */
    public function testRegionCriteria(Country $country, bool $expectedResult, string $region = null)
    {
        if (!isset($region)) {
            $countryCriteria = new CountryCriteria($country);
        } else {
            $countryCriteria = new CountryCriteria($country, $region);
        }

        $this->assertEquals($countryCriteria->checkRegionCriteria(), $expectedResult);
    }

    /**
     * @dataProvider populationCriteriaProvider
     * @param Country   $country
     * @param bool      $expectedResult
     */
    public function testPopulationCriteria(Country $country, bool $expectedResult)
    {
        $countryCriteria = new CountryCriteria($country);
        $this->assertEquals($countryCriteria->checkPopulationCriteria(), $expectedResult);
    }

    /**
     * @dataProvider rivalCriteriaProvider
     * @param Country   $country
     * @param bool      $expectedResult
     */
    public function testRivalCriteria(Country $country, bool $expectedResult)
    {
        $countryCriteria = new CountryCriteria($country);
        $countryCriteria->setRivalCountry(new Country("no", "Norway", "Oslo", "Europe", 150, [0, 0]));

        $this->assertEquals($countryCriteria->checkRivalCriteria(), $expectedResult);
    }

    /**
     * @dataProvider allCriteriaProvider
     * @param Country   $country
     * @param bool      $expectedResult
     */
    public function testValidateAllCriteria(Country $country, bool $expectedResult)
    {
        $countryCriteria = new CountryCriteria($country);
        $countryCriteria->setRivalCountry(new Country("no", "Norway", "Oslo", "Europe", 150, [0, 0]));

        $this->assertEquals($countryCriteria->validateAllCriteria(), $expectedResult);
    }

    public function countryCriteriaProvider()
    {
        return [
            [new Country("c1", "Country1", "Capital1", "Region1", 200, [0, 0])],
            [new Country("c2", "Country2", "Capital2", "Region2", 200, [0, 0])],
        ];
    }

    public function codeCriteriaProvider()
    {
        return [
            [new Country("az", "Country1", "Capital1", "Region1", 200, [0, 0]), true],
            [new Country("Ez", "Country2", "Capital2", "Region2", 200, [0, 0]), true],
            [new Country("cl", "Country3", "Capital3", "Region3", 200, [0, 0]), false],
            [new Country("No", "Country4", "Capital4", "Region4", 200, [0, 0]), false],
        ];
    }

    public function regionCriteriaProvider()
    {
        return [
            [new Country("c1", "Country1", "Capital1", "Europe", 200, [0, 0]), true, null],
            [new Country("c2", "Country2", "Capital2", "OtherRegion", 200, [0, 0]), true, "OtherRegion"],
            [new Country("c3", "Country3", "Capital3", "AnotherOne", 200, [0, 0]), false],
            [new Country("c3", "Country3", "Capital3", "YetAnother", 200, [0, 0]), false, "DifferentRegion"],
        ];
    }

    public function populationCriteriaProvider()
    {
        return [
            [new Country("c1", "Country1", "Capital1", "Asia", 80_000_001, [0, 0]), true],
            [new Country("c2", "Country2", "Capital2", "Asia", 80_000_000, [0, 0]), true],
            [new Country("c2", "Country2", "Capital2", "Asia", 79_999_999, [0, 0]), false],
            [new Country("c1", "Country1", "Capital1", "OtherRegion", 50_000_001, [0, 0]), true],
            [new Country("c2", "Country2", "Capital2", "OtherRegion", 50_000_000, [0, 0]), true],
            [new Country("c2", "Country2", "Capital2", "OtherRegion", 49_999_999, [0, 0]), false],
        ];
    }

    public function rivalCriteriaProvider()
    {
        return [
            [new Country("c1", "Country1", "Capital1", "Region", 100, [0, 0]), false],
            [new Country("c1", "Country1", "Capital1", "Region", 200, [0, 0]), true],
        ];
    }

    public function allCriteriaProvider()
    {
        return [
            [new Country("aa", "Country1", "Capital1", "Europe", 50_000_001, [0, 0]), true],
            [new Country("zz", "Country1", "Capital1", "Europe", 50_000_001, [0, 0]), false],
            [new Country("aa", "Country1", "Capital1", "OtherRegion", 50_000_001, [0, 0]), false],
            [new Country("aa", "Country1", "Capital1", "Europe", 40_000_000, [0, 0]), false],
            [new Country("aa", "Country1", "Capital1", "Europe", 1_000_000, [0, 0]), false],
        ];
    }
}
