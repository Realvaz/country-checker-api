<?php

declare(strict_types=1);

namespace Tests\Application\Actions\Country;

use App\Application\Actions\ActionPayload;
use App\Domain\Country\CountryRepository;
use App\Domain\Country\Country;
use App\Domain\Country\CountryCriteria;
use DI\Container;
use Tests\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class CheckCountryActionTest extends TestCase
{
    use ProphecyTrait;

    public function testAction()
    {
        $app = $this->getAppInstance();

        /** @var Container $container */
        $container = $app->getContainer();

        $country = new Country("c1", "Country1", "Capital1", "Region1", 200, [0, 0]);
        $rivalCountry = new Country("c2", "Country2", "Capital2", "Region2", 100, [0, 0]);
        $countryCriteria = new CountryCriteria($country);
        $countryCriteria->setRivalCountry($rivalCountry);

        $countryRepositoryProphecy = $this->prophesize(CountryRepository::class);

        $countryRepositoryProphecy
            ->findCountryByCode("es")
            ->willReturn($country)
            ->shouldBeCalledOnce();

        $countryRepositoryProphecy
            ->findCountryByCode("no")
            ->willReturn($rivalCountry)
            ->shouldBeCalledOnce();

        $container->set(CountryRepository::class, $countryRepositoryProphecy->reveal());

        $request = $this->createRequest('GET', '/country-check/es');
        $response = $app->handle($request);

        $payload = (string) $response->getBody();
        $expectedPayload = new ActionPayload(200, $countryCriteria);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }
}
