<?php

declare(strict_types=1);

namespace Tests\Infrastructure\http\Country;

class MockedData
{
    static function getMockedData()
    {
        return
            [
                "name" => "Country",
                "topLevelDomain" => [
                    ".zz"
                ],
                "alpha2Code" => "C1",
                "alpha3Code" => "CC1",
                "callingCodes" => [
                    "00"
                ],
                "capital" => "Capital",
                "altSpellings" => [
                    "C1",
                    "C1 Country",
                ],
                "region" => "Region",
                "subregion" => "Southern Region",
                "population" => 10000,
                "latlng" => [0, 0],
                "demonym" => "Countrish",
                "area" => 100000,
                "gini" => 100,
                "timezones" => [
                    "UTC",
                ],
                "borders" => [
                    "C2",
                    "C3",
                ],
                "nativeName" => "Native Country",
                "numericCode" => "0",
                "currencies" => [
                    "Country Curr."
                ],
                "languages" => [
                    "c1"
                ],
                "translations" => [
                    "c2" => "Country 2",
                ],
                "relevance" => "0"
            ];
    }
}
