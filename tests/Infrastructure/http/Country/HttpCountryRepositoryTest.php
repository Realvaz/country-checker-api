<?php

declare(strict_types=1);

namespace Tests\Infrastructure\http\Country;

use App\Domain\Country\Country;
use App\Domain\Country\CountryNotFoundException;
use App\Infrastructure\http\Country\HttpCountryRepository;
use App\Infrastructure\http\HttpClient;
use Prophecy\Argument;
use Tests\TestCase;

/**
 * TODO: Finish the mocked request
 * This test is NOT finished
 */

class HttpCountryRepositoryTest // extends TestCase
{
    public function testfindCountryByCode()
    {
        $app = $this->getAppInstance();

        /** @var Container $container */
        $container = $app->getContainer();

        $country = new Country("c1", "Country", "Capital", "Region", 10000, [0, 0]);

        $countryRepository = new HttpCountryRepository();

        $httpClientCountry = $this->prophesize(HttpClient::class);

        $httpClientCountry
            ->processResponse(Argument::any())
            ->willReturn(MockedData::getMockedData())
            ->shouldBeCalledOnce();

        $container->set(HttpClient::class, $httpClientCountry->reveal());
        $countryRepository->findCountryByCode("es");
        $this->assertEquals($country, $countryRepository->findCountryByCode("c1"));
    }
}
