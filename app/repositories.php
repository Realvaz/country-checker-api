<?php

declare(strict_types=1);

use App\Domain\Country\CountryRepository;
use App\Infrastructure\http\Country\HttpCountryRepository;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    // Here we map our CountryRepository interface to its in memory implementation
    $containerBuilder->addDefinitions([
        CountryRepository::class => \DI\autowire(HttpCountryRepository::class),
    ]);
};
