<?php

declare(strict_types=1);

use App\Application\Settings\Settings;
use App\Application\Settings\SettingsInterface;
use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {

    // Global Settings Object
    $containerBuilder->addDefinitions([
        SettingsInterface::class => function () {
            return new Settings([
                'displayErrorDetails' => true, // Should be set to false in production
                'logError'            => true,
                'logErrorDetails'     => true,
                'logger' => [
                    'name' => 'slim-app',
                    'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
                    'level' => Logger::DEBUG,
                ],
                "checkApiUrl" => [
                    "apiUrl" => $_ENV['CHECK_API_URL'] ?? "",
                    "headers" => [
                        "headers" => [
                            "x-rapidapi-key" => $_ENV["CHECK_API_KEY"] ?? "",
                            "x-rapidapi-host" => "restcountries-v1.p.rapidapi.com"
                        ],
                    ]
                ]
            ]);
        }
    ]);  
};
