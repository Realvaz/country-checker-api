# Country-checker-api

Simple API REST that checks a set of validations when given a country code. If every single criteria is matched with TRUE value, the final result is TRUE as well.

Only endpoint /check-country/{COUNTRY_CODE}

## Installation

Use the package manager [composer](https://getcomposer.org/) to manage dependencies.

1. git clone https://Realvaz@bitbucket.org/Realvaz/country-checker-api.git
2. Create .env with the following variables declared: 
    - CHECK_API_URL (without url parameters)
    - CHECK_API_KEY
3. composer install
4. composer start

With Docker

4. docker-compose up -d

If deployed to production. Change line 16 of app/settings.php

## Usage

Accepts GET requests on /country-check/{COUNTRY_CODE}

For instance:

GET HTTP /country-check/it

Response example:

```json
{
    results: true,
    criteria: {
        code: true,
        region: true,
        population: true,
        rival: true
    }
}
```


## Contributing
Pull requests will not be processed. This project is no longer maintained.