<?php

declare(strict_types=1);

namespace App\Application\Actions\Country;

use Psr\Http\Message\ResponseInterface as Response;
use App\Domain\Country\CountryCriteria;


class CheckCountryAction extends CountryAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $countryCode = (string) $this->resolveArg('code');

        $country = $this->countryRepository->findCountryByCode($countryCode);

        $countryCriteria = new CountryCriteria($country);
        $rival = $this->countryRepository->findCountryByCode($countryCriteria->getRivalCode());
        $countryCriteria->setRivalCountry($rival);

        $remoteAddress = $_SERVER["REMOTE_ADDR"] ?? "unknown";
        $this->logger->info("Country with code `${countryCode}` was checked from IP $remoteAddress.");

        return $this->respondWithData($countryCriteria);
    }
}
