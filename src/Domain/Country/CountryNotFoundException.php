<?php

declare(strict_types=1);

namespace App\Domain\Country;

use App\Domain\DomainException\DomainRecordNotFoundException;

class CountryNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'The country you requested does not exist.';
}
