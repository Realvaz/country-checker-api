<?php

declare(strict_types=1);

namespace App\Domain\Country;

use JsonSerializable;

class CountryCriteria implements JsonSerializable
{
    /**
     * @var Country
     */
    private $country;

    /**
     * @var string
     */
    private $region;

    /**
     * @var string
     */
    private $rivalCode;

    /**
     * @var Country
     */
    private $rivalCountry;

    /**
     * @param Country   $country
     * @param string    $region
     * @param Country   $rivalCountry
     */
    public function __construct(Country $country, string $region = "Europe", string $rivalCode = "no")
    {
        $this->country = $country;
        $this->region = $region;
        $this->rivalCode = $rivalCode;
    }

    /**
     * @return string
     */
    public function getRivalCode()
    {
        return $this->rivalCode;
    }

    /**
     * @return void
     */
    public function setRivalCountry(Country $rivalCountry)
    {
        $this->rivalCountry = $rivalCountry;
    }

    /**
     * @return bool
     */
    public function checkCodeCriteria()
    {
        return preg_match("/^[aeiou]/i", $this->country->getCode()) == 1;
    }

    /**
     * @return bool
     */
    public function checkRegionCriteria()
    {
        return strcasecmp($this->country->getRegion(), $this->region) == 0;
    }

    /**
     * @return bool
     */
    public function checkPopulationCriteria()
    {
        $firstSubcriteria = $this->country->getRegion() == "Asia" && $this->country->getPopulation() >= 80_000_000;
        $secondSubcriteria = $this->country->getRegion() != "Asia" && $this->country->getPopulation() >= 50_000_000;

        return $firstSubcriteria || $secondSubcriteria;
    }

    /**
     * @return bool
     */
    public function checkRivalCriteria()
    {
        return $this->country->greaterPopulationThan($this->rivalCountry) > 0;
    }

    /**
     * @return array
     */
    public function validateAllCriteria()
    {
        return $this->checkCodeCriteria()
            && $this->checkRegionCriteria()
            && $this->checkPopulationCriteria()
            && $this->checkRivalCriteria();
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            "result" => $this->validateAllCriteria(),
            "criteria" => [
                "code" => $this->checkCodeCriteria(),
                "region" => $this->checkRegionCriteria(),
                "population" => $this->checkPopulationCriteria(),
                "rival" => $this->checkRivalCriteria()
            ]
        ];
    }
}
