<?php

declare(strict_types=1);

namespace App\Domain\Country;

use JsonSerializable;

class Country implements JsonSerializable
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $capital;

    /**
     * @var string
     */
    private $region;

    /**
     * @var int
     */
    private $population;

    /**
     * @var array
     */
    private $latlng;

    /**
     * @param string $code
     * @param string $name
     * @param string $capital
     * @param string $region
     * @param int    $population
     * @param array  $latlng
     */
    public function __construct(string $code, string $name, string $capital, string $region, int $population, array $latlng)
    {
        $this->code = strtolower($code);
        $this->name = $name;
        $this->capital = $capital;
        $this->region = $region;
        $this->population = $population;
        $this->latlng = $latlng;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCapital(): string
    {
        return $this->capital;
    }

    /**
     * @return string
     */
    public function getRegion(): string
    {
        return $this->region;
    }

    /**
     * @return string
     */
    public function getPopulation(): int
    {
        return $this->population;
    }

    /**
     * @return string
     */
    public function getLatlng(): array
    {
        return $this->latlng;
    }

    /**
     * @return int
     */
    public function greaterPopulationThan(Country $otherCountry): int
    {
        if ($this->getPopulation() > $otherCountry->getPopulation()) {
            return 1;
        }

        if ($this->getPopulation() < $otherCountry->getPopulation()) {
            return -1;
        }

        return 0;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'code' => $this->code,
            'name' => $this->name,
            'capital' => $this->capital,
            'region' => $this->region,
            'population' => $this->population,
            'latlng' => $this->latlng,
        ];
    }
}
