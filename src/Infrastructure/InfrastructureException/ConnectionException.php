<?php
declare(strict_types=1);

namespace App\Infrastructure\InfrastructureException;

use App\Infrastructure\InfrastructureException\InfrastructureException;

class ConnectionException extends InfrastructureException
{
}
