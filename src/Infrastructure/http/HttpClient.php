<?php

declare(strict_types=1);

namespace App\Infrastructure\http;

use Psr\Http\Message\ResponseInterface;
use \GuzzleHttp\Client;
use \GuzzleHttp\Exception\ConnectException;
use \GuzzleHttp\Exception\RequestException;
use App\Infrastructure\http\HttpConnectionException;

class HttpClient
{
    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param string $URL
     * @param string $params
     * @return string
     */
    public function buildQueryParams(string $URL, array $params = []): string
    {
        return $URL . "?" . http_build_query($params);
    }

    /**
     * @param string $URL
     * @param string $method
     * @param array $params
     * @param array $options
     * @return ResponseInterface
     * @throws TODO
     */
    public function makeRequest(
        string $method = "GET",
        string $URL,
        array $params = [],
        array $options = []
    )
    {
        $urlWithParams = $this->buildQueryParams($URL, $params);
        try {
            $response = $this->client->request($method, $urlWithParams, $options); 
        } catch (ConnectException | RequestException $e) {
            throw new HttpConnectionException();
        }

        return $response;
    }

    /**
     * @param ResponseInterface $response
     * @return array
     */
    public function processResponse(ResponseInterface $response): array
    {
        $body = json_decode($response->getBody()->getContents(), true);
        return $body;
    }
}
