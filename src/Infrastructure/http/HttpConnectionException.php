<?php

declare(strict_types=1);

namespace App\Infrastructure\http;

use App\Infrastructure\InfrastructureException\ConnectionException;

class HttpConnectionException extends ConnectionException
{
    public $message = 'The service is currently unavailable. Please try again later. If problems continue, contact support.';
}
