<?php

declare(strict_types=1);

namespace App\Infrastructure\http\Country;

use App\Domain\Country\Country;
use App\Domain\Country\CountryNotFoundException;
use App\Domain\Country\CountryRepository;
use App\Infrastructure\http\HttpClient;
use App\Infrastructure\BaseRepository;
use App\Application\Settings\SettingsInterface;

class HttpCountryRepository extends BaseRepository implements CountryRepository
{
    /**
     * @var Country
     */
    private $country;

    /**
     * {@inheritdoc}
     */
    public function findCountryByCode(string $code): Country
    {        
        $settings = $this->container->get(SettingsInterface::class)->get("checkApiUrl");

        $client = new HttpClient();
        $response = $client->makeRequest("GET", $settings["apiUrl"], ["codes" => $code], $settings["headers"]);
        $data = $client->processResponse($response)[0];
        
        if (!isset($data)) {
            throw new CountryNotFoundException;
        }

        $this->country = new Country(
            $code,
            $data["name"],
            $data["capital"],
            $data["region"],
            $data["population"],
            $data["latlng"]
        );

        return $this->country;
    }
}