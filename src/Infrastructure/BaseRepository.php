<?php

namespace App\Infrastructure;

use Psr\Container\ContainerInterface;

class BaseRepository {

    protected $container;

    public function __construct(ContainerInterface $c) {
        $this->container = $c;
    }
}